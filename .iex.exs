alias EchatEngine.Message

send_receive = fn chat, msg ->
  EchatEngine.send_message(chat, %Message{text: msg})
  Process.info(self(), :messages)
  #receive do x -> x end
end
