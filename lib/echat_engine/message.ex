defmodule EchatEngine.Message do
  use EchatEngine.Schema
  import Ecto.Changeset
  alias EchatEngine.{Chat, Message}

  embedded_schema do
    field :author_name, :string
    field :text, :string
    belongs_to :chat, Chat
    timestamps updated_at: false
  end

  def changeset(change, params) do
    change
    |> cast(params, [:chat_id, :author_name, :text, :inserted_at])
    |> validate_required([:chat_id, :author_name, :text])
    |> validate_length(:author_name, max: 30)
    |> validate_length(:text, max: 300)
  end

  def try_params(params) do
    %Message{}
    |> changeset(params)
    |> apply_action(:insert)
  end
end
