defmodule EchatEngine.Schema do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      @primary_key {:id, :string, []}
      @foreign_key_type :string

      @timestamps_opts [type: :utc_datetime_usec]
    end
  end
end
