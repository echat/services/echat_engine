defmodule EchatEngine.ChatServer do
  use GenServer, restart: :transient
  require Logger
  alias EchatEngine.{ChatRegistry, ChatServer, Message}

  defstruct [:id, :messages]

  @max_messages 10
  @max_message_age 60 # in seconds

  defp via(key) do
    {:via, Registry, {ChatRegistry, key}}
  end
  
  def start_link(args) do
    args =
      if Keyword.has_key?(args, :id) do
        args
      else
        Keyword.put(args, :id, random_id())
      end

    id = Keyword.get(args, :id)

    GenServer.start_link(__MODULE__, args, name: via(id))
  end

  defp random_id() do
    :crypto.strong_rand_bytes(50) |> Base.url_encode64(padding: false)
  end

  def init(args) do
    id   = Keyword.get(args, :id)

    {:ok, %ChatServer{id: id, messages: []}}
  end

  def handle_call({:message, message}, _from, state) do
    messages = [message | state.messages]
               |> Enum.take(@max_messages)
               |> Enum.filter(fn %Message{inserted_at: t} ->
                 DateTime.diff(DateTime.utc_now(), t) < @max_message_age
               end)

    {:reply, :ok, %{state | messages: messages}}
  end

  def handle_call(:load, _from, state) do
    {:reply, state.messages, state}
  end

  def handle_call(:id, _from, state) do
    {:reply, state.id, state}
  end

  def send_message(chat, %Message{} = message), do:
    GenServer.call(chat, {:message, message})

  def load(chat), do:
    GenServer.call(chat, :load)

  def get_id(pid), do:
    GenServer.call(pid, :id)
end
