defmodule EchatEngine.Chat do
  use EchatEngine.Schema
  import Ecto.Changeset
  alias EchatEngine.Message

  embedded_schema do
    field :topic, :string
    field :description, :string
    has_many :messages, Message
    timestamps()
  end

  def changeset(chat, params) do
    chat
    |> cast(params, [:topic, :description])
    |> validate_required([])
    |> validate_length(:topic, max: 30)
    |> validate_length(:description, max: 300)
  end
end
