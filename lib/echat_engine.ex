defmodule EchatEngine do
  alias EchatEngine.{ChatRegistry, PubSub, ChatServer, Message, ChatSupervisor}
  alias Phoenix.PubSub

  def connect_to_chat(chat_id) do
    PubSub.subscribe(EchatEngine.PubSub, "chat:#{chat_id}")
    start_chat(chat_id)
  end

  def new_random_chat() do
    {:ok, pid} = DynamicSupervisor.start_child(ChatSupervisor, {ChatServer, []})
    ChatServer.get_id(pid)
  end

  defp start_chat(chat_id) do
    pid = get_chat(chat_id)
    if pid do
      {:ok, pid}
    else
      DynamicSupervisor.start_child(ChatSupervisor, {ChatServer, [id: chat_id]})
    end
  end

  defp get_chat(chat_id) do
    match_all = {:"$1", :"$2", :"$3"}
    guards = [{:"==", :"$1", chat_id}]
    map_result = [:"$2"]

    Registry.select(ChatRegistry, [{match_all, guards, map_result}])
    |> List.first
  end

  def get_all_chats() do
    match_all = {:"$1", :"$2", :"$3"}
    guards = []
    map_result = [:"$2"]

    Registry.select(ChatRegistry, [{match_all, guards, map_result}])
    |> Enum.map(&EchatEngine.ChatServer.get_id/1)
  end

  def send_message(chat_id, message_params) do
    message_params = message_params
                     |> Map.put(:chat_id, chat_id)
                     |> Map.put(:inserted_at, DateTime.utc_now())

    with {:ok, message} <- Message.try_params(message_params) do
      {:ok, pid} = start_chat(chat_id)
      PubSub.broadcast(EchatEngine.PubSub,
        "chat:#{chat_id}",
        {:message, message}
      )
      ChatServer.send_message(pid, message)
      
      :ok
    else
      err -> err
    end
  end

  def load_messages(chat_id) do
    {:ok, pid} = start_chat(chat_id)
    ChatServer.load(pid)
  end
end
