defmodule EchatEngine.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    host_runner_config = [
      strategy: :one_for_one,
      max_seconds: 30,
      name: EchatEngine.ChatSupervisor
    ]

    children = [
      {Phoenix.PubSub, name: EchatEngine.PubSub},
      {Registry, keys: :unique, name: EchatEngine.ChatRegistry},
      {DynamicSupervisor, host_runner_config},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: EchatEngine.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
